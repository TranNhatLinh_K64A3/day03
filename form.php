<?php
    $sex = Array(
        0 => "Nam",
        1 => "Nữ"
    );

    $department = Array(
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    )
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="theme.css">
    <title>Document</title>
</head>
<body>
    <div class = "wrapper">
        <form action="">
            <div class = "name_input">
                <label>Họ và tên</label>
                <input class="input_name" type="text">
            </div>

            <div class = "sex_select">
                <label>Giới tính</label>
                <?php
                    for ($i = 0; $i < 2; $i++) {
                        echo "
                            <input class=\"input_sex\" name = \"sex\" type=\"radio\">{$sex[$i]}
                        ";
                    }
                ?>
            </div>

            <div class = "department">
                <label>Phân khoa</label>
                <select class="selection" style="width:140px;">
                    <?php
                        foreach (["", "MAT", "KDL"] as $dep) {
                            echo '
                                <option value="' . $dep . '">'.$department[$dep].'</option>
                            ';
                        }
                    ?>
                </select>
            </div>

            <button>Đăng ký</button>
        </form>
    </div>

</body>
</html>